import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { Todo } from './todo';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class TodoService {
    readonly API_END_POINT: string = 'http://127.0.0.1:1323/todo';
    todos: Todo[] =  [{id: "", topic: ""}]; 
    private headers = new Headers({'Content-Type': 'application/json'});

    constructor(private http: Http){
        this.getTodos().then(todos => this.todos = todos);
    }

    getTodos(): Promise<Todo[]> {
        return this.http.get(this.API_END_POINT)
            .toPromise()
            .then(response => response.json() as Todo[])
            .catch(this.handleError);
    }

    newTodo(model: Todo) {
        this.http
            .post(this.API_END_POINT, 
            JSON.stringify(
                {id: model.id, topic: model.topic}), 
                {headers: this.headers}
            )
            .toPromise()
            .then(() => {this.getTodos().then(todos => this.todos = todos);})
            .catch(this.handleError);
    }    

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}