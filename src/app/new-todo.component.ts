import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo-service'
import { Todo } from './todo';

@Component({
    selector: 'app-new-todo',
    template: `
        <div>
            <form #f="ngForm" (ngSubmit)="newTodo(f.value)">
                <input type="text" name="id" [(ngModel)]="todo.id">
                <input type="text" name="topic" [(ngModel)]="todo.topic">
                <button type="submit"> New Todo</button>
            </form>
        </div>
    `
})
export class NewTodo implements OnInit{
    todo:Todo
    constructor(private todoService: TodoService) { }  
    
    ngOnInit() {
        this.todo = {id:"", topic:""};
    }

    newTodo(model: Todo){
        this.todoService.newTodo(model);
    }
}