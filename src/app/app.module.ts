import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ListTodo } from './list-todo.component' 
import { NewTodo } from './new-todo.component'

import { TodoService } from './todo-service';

@NgModule({
  declarations: [
    AppComponent,
    ListTodo,
    NewTodo
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
