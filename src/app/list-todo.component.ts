import { Component, OnInit } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { TodoService } from './todo-service';
import { Todo } from './todo';

@Component({
    selector:'app-todo-list',
    template: `
    <li *ngFor="let todo of todoService.todos">
        <span class="badge">{{todo.id}}:</span> {{todo.topic}}
    </li>`
})
export class ListTodo {
    constructor(private todoService: TodoService) {}  
}