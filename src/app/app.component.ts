import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  private headers = new Headers({'Content-Type': 'application/json'});

  title = 'Toto List';
}
